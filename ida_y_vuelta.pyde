
d = 20
valor = '#8408ff'
sentido = 1
valorInverso = '#8408ff'

def setup():
    global valorInverso
    size(500, 500)
    valorInverso = color(255 - red(valor), 255 - green(valor), 255 - blue(valor))
    

def draw():
    global d, sentido
    background(valor)
    
    fill(255, 100)
    noStroke()
    ellipse(0, 250, d, d)
    ellipse(500, 250, d, d)
    ellipse(250, 0, d, d)
    ellipse(250, 500, d, d)
    
    d += 8 * sentido
     
    if d > 500 or d < 20:
        sentido *= -1
        
    fill(valorInverso)
    ellipse(250, 250, 12, 12)
    
    if mousePressed:
        ellipse(mouseX, mouseY, 12, 12)
        stroke(valorInverso)
        strokeWeight(3)
        line(mouseX, mouseY, 250, 250)
        
